---
layout: page
title: About
permalink: '/about'
---

Hey there! 👋

I'm Ammar, a web developer living in lovely Istanbul 🕌.

- Currently working at [GitLab](https://about.gitlab.com/) as a [Senior Frontend Engineer](https://about.gitlab.com/job-families/engineering/frontend-engineer/).

- BSc. in Computing from [UoG](https://www.gre.ac.uk/), and started working in web development in 2009.

- Besides web development, I like video games, Arabic calligraphy, and petting cats and birds.

- Find me on [Twitter](https://twitter.com/AmmarCodes), [GitHub](https://github.com/AmmarCodes), [GitLab](https://gitlab.com/aalakkad), and [LinkedIn](https://www.linkedin.com/in/aalakkad/). Or drop me an email at `am.alakkad` @ gmail.
