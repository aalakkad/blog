# Ammar Alakkad blog

## Image caption (figure)

Use the following snippet:

```liquid
{% include image.html url="/assets/images/uses.png" description="Screenshot of iTerm running tmux and nvim with Palenight theme" %}
```
