---
layout: post
title: Rebase first two commits
excerpt: How to rebase first two commits
image: /assets/images/git.png
date: 2020-05-30 00:00 +0000
tags:
  - tips
  - til
  - git
redirect_from:
  - /posts/til-rebase-first-two-commits/
---

I ran into a situation where I have multiple commits, but I wanted to squash the second commit with the first one.

If you're trying to do the regular `git rebase -i master`, it will list all commits except the first one.

To list all commits including the first one, this could be used:

```bash
git rebase -i --root master
```

[reference](https://stackoverflow.com/a/598788)
