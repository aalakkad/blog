---
layout: post
title: Picking hobbies
excerpt: Why you want to have a hobby
image: /assets/images/hobbies.jpg
date: 2020-04-28 00:00 +0000
redirect_from:
  - /posts/picking-hobbies/
---

![Photo by Steve Johnson on Unsplash.com)](/assets/images/hobbies.jpg)

We all should have hobbies, right?

Well, it's right, but the thing is that some of us might not know what hoppies do that have.
It could be due to some stress, anxiety, or simply because they didn't have the time and intention to discover what they like.

## Why having hobbies is important

How would you relax and have a good down time? people might go to YouTube or Netflix to spend the time there because they don't anything else to do instead, but why not spend the time doing something you really like? wouldn't that bring you more joy?

## Picking the hobby

If you're searching for that hobby, try doing one hobby for couple days and see if you enjoy it.

Here's some examples of hobbies you might be interested in trying out:

- Baking
- Calligraphy
- Coffee roasting
- Coloring
- Programming
- Digital art
- Gaming
- Distro hopping
- Knitting
- Jigsaw puzzles
- Origami
- Pet adoption
- Photography
- Playing musical instrument
- Wood carving

### Resources

- [List of hobbies - Wikipedia](https://en.wikipedia.org/wiki/List_of_hobbies)
- [The Ultimate List of Hobbies – 541 handpicked hobby ideas to try](https://www.mantelligence.com/list-of-hobbies/)

> Post photo by [Steve Johnson](https://unsplash.com/@steve_j) on [Unsplash](https://unsplash.com)
