---
layout: post
title: 'Terminal: navigate to previous folder or home'
date: 2016-07-08 00:00 +0000
excerpt: How to navigate to previous folder or home in terminal.
image: /assets/images/navigation-to-previous-folder-and-home.gif
tags:
  - command line
  - tips
redirect_from:
  - /posts/terminal-navigate-to-previous-folder-or-home/
---

![Navigation to previous folder and home](/assets/images/navigation-to-previous-folder-and-home.gif)

To navigate to the latest folder you were in, use: "cd -".
To navigate to the home folder use: "cd" without parameters.
