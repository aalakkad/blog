---
layout: post
title: 'Terminal: navigate to beginning and ending of line'
excerpt: How to navigate to the beginning and end of a line in terminal.
date: 2016-07-11 00:00 +0000
image: /assets/images/navigation-to-beginning-and-end-of-line.gif
tags:
  - command line
  - tips
---

![Navigation to beginning and end of line](/assets/images/navigation-to-beginning-and-end-of-line.gif)

Sometimes it's handy to go the beginning of the line, maybe you want to add a forgotten "sudo"? or to move to the end of the line to add some arguments?

To navigate to the beginning of the line in use: "CTRL+a".
To navigate to the end of the line in use: "CTRL+e".

**Hint**: the shortcuts works on most of the applications inside the terminal, like Vim, Emacs and others.
